<?php

namespace App\Services;

use Illuminate\Support\Collection;
use App\Entity\User;
use App\Requests\SaveUserRequest;

class UserService implements UserServiceInterface
{

    public function findAll(): Collection
    {
        return User::all();
    }

    public function findById(int $id): ?User
    {
        return User::find($id);
    }

    public function save(SaveUserRequest $request): User
    {
        if($request->getId() && $user = $this->findById($request->getId())){
            $user->update([
                'id' => $request->getId(),
                'name' => $request->getName(),
                'email' => $request->getEmail()
            ]);
        } else{
            $user = User::create([
                'id' => $request->getId(),
                'name' => $request->getName(),
                'email' => $request->getEmail()
            ]);
        }
        return $user;
    }

    public function delete(int $id): void
    {
        User::destroy($id);
    }
}