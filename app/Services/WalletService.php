<?php

namespace App\Services;

use App\Entity\User;
use Illuminate\Support\Collection;
use App\Entity\Wallet;
use App\Requests\CreateWalletRequest;

class WalletService implements WalletServiceInterface
{
    public function findByUser(int $userId): ?Wallet
    {
        return Wallet::where('user_id', $userId)->first();
    }

    public function create(CreateWalletRequest $request): Wallet
    {
        if (Wallet::where('user_id', $request->getUserId())->first()) {
            throw new \LogicException('There can not be more than 1 record with the same user_id');
        }
        return Wallet::create(['user_id' => $request->getUserId()]);
    }

    public function findCurrencies(int $walletId): Collection
    {
        $wallet = Wallet::find($walletId);
        return $wallet->currencies()->get();
    }
}