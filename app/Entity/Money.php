<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Money extends Model
{
    use SoftDeletes;

    protected $table = 'money';

    protected $guarded = [];

    protected $dates = ['deleted_at'];

    public $timestamps = false;
}
