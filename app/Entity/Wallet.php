<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wallet extends Model
{
    use SoftDeletes;

    protected $table = 'wallet';

    protected $guarded = [];

    protected $dates = ['deleted_at'];

    public $timestamps = false;

    public function currencies()
    {
        return $this->belongsToMany(Currency::class, 'money');
    }
}
