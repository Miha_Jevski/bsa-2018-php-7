<?php

use App\Entity\Money;
use Faker\Generator as Faker;

$factory->define(Money::class, function (Faker $faker) {
    return [
        'currency_id' => $faker->numberBetween(1,10),
        'amount' => $faker->randomFloat(2,500, 10000),
        'wallet_id' => $faker->numberBetween(1,10)
    ];
});
